<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Case_Status_In_Progress</fullName>
        <description>Updates the Case status to &quot;In Progress&quot;</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Case Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Reminder_Date</fullName>
        <description>Reset the Reminder date to NULL</description>
        <field>Reminder_Date__c</field>
        <name>Reset Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_as_Re_Opened</fullName>
        <description>Update Case Status as &apos;Re-Opened&apos;</description>
        <field>Status</field>
        <literalValue>Re-Opened</literalValue>
        <name>Update Status as Re-Opened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case %3A Re-Open Case when Customer Reples</fullName>
        <actions>
            <name>Update_Status_as_Re_Opened</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Complaints,Enquiry,Technical Support,YourOrder</value>
        </criteriaItems>
        <description>Re-open a Case when Customer replies. This is for cases which are created using Email to case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer replied Update Case Status</fullName>
        <actions>
            <name>Reset_Reminder_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ADSL to NBN Transitions</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.FromAddress</field>
            <operation>notContain</operation>
            <value>belong.com.au</value>
        </criteriaItems>
        <description>update the Case status when a customer has replied</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
