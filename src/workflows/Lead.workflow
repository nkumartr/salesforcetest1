<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Abandoned_Cart_Lead_Email</fullName>
        <description>Abandoned Cart Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Abandoned_Cart_Lead_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_Notification</fullName>
        <ccEmails>emailtosalesforce@2y8emqiy4omvocq2nmftp1xiqjz4amguzpaz6n0a6x2bldglw0.28-12ocheay.ap2.le.salesforce.com</ccEmails>
        <description>Email Alert Notification</description>
        <protected>false</protected>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Abandoned_Cart_Lead_Email_Lead</template>
    </alerts>
    <alerts>
        <fullName>Email_sent_to_a_customer_once_they_are_created_as_a_lead_in_salesforce_due_to_ab</fullName>
        <description>Email sent to a customer once they are created as a lead in salesforce due to abandoning their cart ie not progressing through to payment</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@belong.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Belong_Email_Templates/Join_Drop</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_EDM_opt_out</fullName>
        <field>Electronic_Direct_Marketing_Opt_Out__c</field>
        <literalValue>0</literalValue>
        <name>Clear EDM opt out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Escalated</fullName>
        <field>Escalated__c</field>
        <literalValue>1</literalValue>
        <name>Lead Escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EDM_Opt_out_box</fullName>
        <field>Electronic_Direct_Marketing_Opt_Out__c</field>
        <literalValue>1</literalValue>
        <name>Update EDM Opt out box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EOI Address Not Found - Escalation</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>EOI - Address Not Found</value>
        </criteriaItems>
        <description>If lead type is EOI Address Not Found and status is Received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Escalated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>External List - Escalation</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>External Lists</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Escalated__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notEqual</operation>
            <value>Data Services</value>
        </criteriaItems>
        <description>If lead type is External List and status is Received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Escalated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Join Form Drop Out - Escalation</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Join Form Drop Out</value>
        </criteriaItems>
        <description>If lead type is Join Form Drop Out and status is Received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Escalated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead Creation Email for Join Form Drop Out</fullName>
        <actions>
            <name>Abandoned_Cart_Lead_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Join Form Drop Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Belong Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Belong Integration Old</value>
        </criteriaItems>
        <description>Email workflow to customers who fail to proceed to payment on the website (Join form Drop Out)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Creation Email for Join Form Drop Out_HTML</fullName>
        <actions>
            <name>Email_sent_to_a_customer_once_they_are_created_as_a_lead_in_salesforce_due_to_ab</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Join Form Drop Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Belong Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Belong Integration Old</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Belong Administrator</value>
        </criteriaItems>
        <description>An Email sent to customers inviting them back to register with Belong</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Opt Out</fullName>
        <actions>
            <name>Update_EDM_Opt_out_box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Re subscribe</fullName>
        <actions>
            <name>Clear_EDM_opt_out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
ISCHANGED( HasOptedOutOfEmail ) , 
HasOptedOutOfEmail = false 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Product Sales - Escalation</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Product Sales Online Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <description>If lead type is Product Sales Online Form and status is Received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Escalated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SQ Down - Escalation</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>EOI - SQ Down</value>
        </criteriaItems>
        <description>If lead type is SQ Down and status is Received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Escalated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
