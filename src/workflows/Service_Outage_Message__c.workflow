<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Outbound_Message</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>https://www.belong.com.au/api/services/salesforce/serviceoutge/notification</endpointUrl>
        <fields>Active__c</fields>
        <fields>Id</fields>
        <fields>Message_Description__c</fields>
        <fields>Name</fields>
        <fields>State__c</fields>
        <fields>Tech_Type__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>belongadmin@belong.com.au</integrationUser>
        <name>Outbound Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Service Outage Message</fullName>
        <actions>
            <name>Outbound_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>IF((ISNEW() &amp;&amp; Active__c )|| (ISCHANGED(Tech_Type__c)) || (ISCHANGED(State__c)) || (ISCHANGED(Message_Description__c)) || (ISCHANGED(Active__c)) , true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
