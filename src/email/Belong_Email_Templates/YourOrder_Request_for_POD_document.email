Hello {!Contact.FirstName},

We have identified an active service at the address you have requested for connection. Before we can process your order, we need proof that you are living at the address you are wishing to connect. We call this a Proof of Occupancy Document. This authorises Belong to disconnect the previous tenant’s existing service on your behalf. 


Providing the right Proof of Occupancy Document

Please send a copy of the document(s) from one of the following categories to yourorder@belong.com.au:

1)	Lease/Rental Agreement*; or
2)	Contract of Sale*; or
3)	Statutory declaration^ from yourself and a utility bill (eg rates, power, water) ; or
4)	Statutory declaration^ from the property owner and the rent receipt from yourself; or
5)	Statutory declaration^ from the property owner and the bond payment receipt; or
6)	Letter from the Real Estate Agent and the rent receipt from yourself; or
7)	Letter from the Real Estate Agent and the bond payment receipt

Please only send through pages which consist of following details:
•	Full Name
•	Service Address
•	Lease/Rental Commencement date or Settlement date

If you are attaching a photo, please ensure the image is taken directly over the entire document to include all of the above information and is clear and legible. This will minimise delays with progressing with your connection otherwise resubmission will be required.

Note:
* If your name is different to your account details please include a statutory declaration to explain the difference with supporting documentation such as a drivers licence or marriage certificate.
^ A statutory declaration must be signed by an appropriate witness e.g. Justice of the Peace


Your connection time

Once the document(s) have been received and validated, we will confirm this via email and your service will generally be connected within 10 business days. 

If you have questions please don't hesitate to reply to this email or give us a call on 1300 235 664.

We look forward to having you on board soon. 
The Belong team