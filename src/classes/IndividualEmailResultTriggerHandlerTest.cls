@isTest
Public class IndividualEmailResultTriggerHandlerTest{

        Public static testMethod void UpdateEmailSentTest(){
      //  Profile p =[Select id, name from Profiles where name='System Administrator'];
        
        Map<String,Schema.RecordTypeInfo>accRecordTypeInfoName = GlobalUtil.getRecordTypeByName('Account');
        
        Account sAccount = new Account();
        sAccount.RecordTypeId = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name = 'Test Account';
        sAccount.Phone = '0412345678';
        sAccount.Customer_Status__c = 'Active';
        sAccount.Octane_Customer_Number__c = '123456';
        insert sAccount;
        
        Contact sContact = new Contact();
        sContact.FirstName = 'Joe';
        sContact.LastName = 'Belong';
        sContact.AccountId = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        Contact Con= [Select id from contact where id=:sContact.id limit 1];
        string conid= con.id;
        
        et4ae5__SendDefinition__c esend = new et4ae5__SendDefinition__c(et4ae5__MemberID__c='7275899',
                                                                        et4ae5__SendClassificationId__c='db8c1c6b-b616-e611-a2e3-1402ec653a5c',
                                                                         et4ae5__JobId__c='15036',
                                                                            et4ae5__EmailId__c='162',
                                                                            et4ae5__ConversationId__c='00D280000012oCHa4v280000004ObQ');
           insert esend;
            et4ae5__SendDefinition__c esend1 = new et4ae5__SendDefinition__c(et4ae5__MemberID__c='7275899000',
                                                                        et4ae5__SendClassificationId__c='db8c1c6b-b616-e611-a2e3-1402ec653a5c',
                                                                         et4ae5__JobId__c='15038',
                                                                            et4ae5__EmailId__c='163',
                                                                            et4ae5__ConversationId__c='00D280000012oCHa4v280000004ObP');
           insert esend1;
        
        List<Marketing_Service_Email__c> lmse = new List<Marketing_Service_Email__c>();
            Marketing_Service_Email__c m1= new Marketing_Service_Email__c(Name='Test Mail',
                                                                          JobID__c='15036',
                                                                          Email_Link__c='http://something.something.com',
                                                                          Contact__c=sContact.id);
            Marketing_Service_Email__c m2= new Marketing_Service_Email__c(Name='Test Mail',
                                                                          JobID__c='15038',
                                                                          Email_Link__c='http://something.something.com',
                                                                          Contact__c=sContact.id);  
              lmse.add(m1);
              lmse.add(m2);
              insert lmse;
        
        List<et4ae5__IndividualEmailResult__c> lIer = new List<et4ae5__IndividualEmailResult__c>();
        
        et4ae5__IndividualEmailResult__c e1 = new et4ae5__IndividualEmailResult__c(Name='Test Name',
                                                                                   et4ae5__Contact__c=sContact.id,
                                                                                   et4ae5__SubjectLine__c='test subject',
                                                                                   et4ae5__FromAddress__c='a@b.com',
                                                                                   et4ae5__MergeId__c=esend.ID+Conid,
                                                                                   et4ae5__SendDefinition__c=esend.ID,
                                                                                   et4ae5__FromName__c='Test');
       et4ae5__IndividualEmailResult__c e2 = new et4ae5__IndividualEmailResult__c(Name='Test Name new',
                                                                                   et4ae5__Contact__c=sContact.id,
                                                                                   et4ae5__SubjectLine__c='test subject new',
                                                                                   et4ae5__MergeId__c=esend1.ID+Conid,
                                                                                   et4ae5__FromAddress__c='a@b.com',
                                                                                   et4ae5__FromName__c='Test',
                                                                                   et4ae5__SendDefinition__c=esend1.ID);
              lIer.add(e1);
              lIer.add(e2);
              insert lIer;
              Update e1;
              update e2;
              
              
              
                                                                           
                           
        }


}