/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 12/05/2016
  @Description : Controller Class for Belong Support Search Results PKB .
*/
public with sharing class BelongSupportSearchResultsController 
{
    /* ***** HANDLE TO CURRENT INSTANCE OF CONTROLLER (to be passed to rendered VF components, avoids re-instantiation of controller) ***** */
	public BelongSupportSearchResultsController pkbsearchCon { get { return this; } }
	
    public static String DEFAULT_PUBLISH_STATUS = 'online';
    public static String REGEX_SEARCH_TERMS = '[^a-zA-Z0-9\'& ]';
	public static String DEFAULT_SITE_NAME = Site.getName();
	
	public String searchResultTerms 	{ get; set; }
	public String FAQTitle 				{ get; set; }
	public Integer FAQResultSize 		{ get; set; }
	private String dataCategoryAPI 		{ get; set; }
	
	public String keywordsMeta 			{ get; set; }
	public String descriptionMeta 		{ get; set; }
	public String titleMeta 			{ get; set; }
	public String urlMetaOG 			{ get; set; }
	public String keywordsMetaOG 		{ get; set; }
	public String descriptionMetaOG 	{ get; set; }
	public String titleMetaOG 			{ get; set; }
	public String imageMetaOG 			{ get; set; }
	
	public FAQ__kav faqTopicArticle 	{ get; set; }
	public String publishStatus 		{ get { return DEFAULT_PUBLISH_STATUS; } }
	public String siteName 				{ get { return DEFAULT_SITE_NAME; } }
	public Boolean isSite 				{ get { return String.isNotBlank(Site.getName()); } }
	
	public Map<String,List<Public_Knowledge_Product__c>> knowledgeProducts 			{ get; set; }
	public Map<String,Public_Knowledge_URL_Mapping__c> knowledgeURL 				{ get; set; }
	public Map<String,List<String>> prodNameProdCate								{ get; set; }
	public List<Public_Knowledge_Product_Display_Setting__c> knowledgeSettingProd 	{ get; set; }
	public Public_Knowledge_URL_Mapping__c knowledgeMetaSetting						{ get; set; }
	
	public List<FAQ__kav> 				FAQArticles 		{ get; set; }
	public Map<String, List<FAQ__kav>> 	catNameFAQs 		{ get; set; }
	public Map<String, String> 			catAPINameTitle		{ get; set; }
	public List<String> 				displayFAQResults	{ get; set; }
	
	public String currentSiteUrl 
  	{
    	set;
    	get 
    	{
      		if (currentSiteUrl == null) 
      		{
        		currentSiteUrl = Site.getBaseUrl()+'/';//Site.getCurrentSiteUrl();
        		if (!isSite) 
        		{
        			currentSiteUrl = Page.Belong_Support_Home.getUrl() + '/';
        		}
      		}
      		return currentSiteUrl;
    	}
	}
	
	public String pageTitle 
	{
    	get 
    	{
      		String t = 'Belong Support - Search';
      		return t;
    	}
  	}
	public BelongSupportSearchResultsController(){}
	public BelongSupportSearchResultsController(ApexPages.StandardController sc) 
	{
		Map<String, String> labelProducts 	= new Map<String, String>();
		prodNameProdCate = new Map<String,List<String>>();
		catNameFAQs 	 = new Map<String, List<FAQ__kav>>();
		catAPINameTitle  = new Map<String, String>();
		displayFAQResults = new List<String>();
		
		knowledgeSettingProd = KnowledgeSiteUtil.buildSettingsDisplay();
		knowledgeProducts 	 = KnowledgeSiteUtil.buildProductDisplay();
		knowledgeURL 		 = KnowledgeSiteUtil.buildURLMapping();
		
		system.debug(logginglevel.error, 'BelongSupportSearchResultsController');
		
		for(Public_Knowledge_Product_Display_Setting__c pKwDispProd : knowledgeSettingProd)
		{
			labelProducts.put(pKwDispProd.Name, pKwDispProd.Product_Label__c);
		}
		
		
		for(List<Public_Knowledge_Product__c> kwProdList : knowledgeProducts.values())
		{
			for(Public_Knowledge_Product__c kwProd : kwProdList)
			{
				String title = labelProducts.get(kwProd.Product_Category__c) + ' > ' + kwProd.Title__c;
				String dataCat = kwProd.Data_Category__c.substring(0, kwProd.Data_Category__c.length()-3); 
				catAPINameTitle.put(dataCat , title);
				
				if(!prodNameProdCate.containsKey(kwProd.Product_Category__c))
				{
					prodNameProdCate.put(kwProd.Product_Category__c, new List<String>());
				}
				prodNameProdCate.get(kwProd.Product_Category__c).add(dataCat);
			}
		}
 
		if (ApexPages.currentPage().getParameters().containsKey('search'))
		{
			if(String.IsNotBlank(ApexPages.currentPage().getParameters().get('search')))
			{
				searchResultTerms = ApexPages.currentPage().getParameters().get('search');
				getSearchArticle();
			}	
		}
		
		constructMetadata();
	}
	
	public void getSearchArticle()
	{
		if(String.IsBlank(searchResultTerms))
			return;
		
		Map<id, FAQ__kav> faqIdFAQobj = new Map<id, FAQ__kav>();
		String searchquery;
		catNameFAQs.clear();
		String normalizeSearchResultTerms = searchResultTerms.Replaceall(REGEX_SEARCH_TERMS,'');
		searchquery = 'FIND \'' + '*' + String.escapeSingleQuotes(normalizeSearchResultTerms) + '*' + '\' ' +
		'IN ALL FIELDS Returning ' +
		'FAQ__kav(id, Title, UrlName, Summary, 	Normalize_URL_Name__c WHERE PublishStatus = \'Online\' AND Language = \'en_US\' AND IsVisibleInPkb = true LIMIT 20)' +
		'WITH DATA CATEGORY Category__c BELOW (NBN__c, ADSL__c, Voice__c)  UPDATE TRACKING';
		system.debug(logginglevel.error, 'BelongSupportSearchResultsController @searchquery' + searchquery);
		List<List<SObject>>searchList = search.query(searchquery);
		
		system.debug(logginglevel.error, 'BelongSupportSearchResultsController @searchList' + searchList);
		
		List<FAQ__kav> FAQResults = (List<FAQ__kav>)searchList[0];
		system.debug(logginglevel.error, 'BelongSupportSearchResultsController @FAQResults' + FAQResults);
		
		for(FAQ__kav faqObj : FAQResults)
		{
			faqIdFAQobj.put(faqObj.Id, faqObj);
		}
		
		system.debug(logginglevel.error, 'BelongSupportSearchResultsController @faqIdFAQobj' + faqIdFAQobj);
		
		FAQResultSize = FAQResults.size();
		for(FAQ__DataCategorySelection fadCatObj : [SELECT DataCategoryGroupName,DataCategoryName,Id,ParentId 
													FROM FAQ__DataCategorySelection
													WHERE ParentId IN: faqIdFAQobj.keySet()])
		{
			system.debug(logginglevel.error, 'BelongSupportSearchResultsController @fadCatObj' + fadCatObj);
			if(fadCatObj.DataCategoryName == 'NBN' || fadCatObj.DataCategoryName == 'ADSL' || fadCatObj.DataCategoryName == 'Voice' )
				continue;
			if(!catNameFAQs.containsKey(fadCatObj.DataCategoryName))
				catNameFAQs.put(fadCatObj.DataCategoryName, new List<FAQ__kav>());
			
			catNameFAQs.get(fadCatObj.DataCategoryName).add(faqIdFAQobj.get(fadCatObj.ParentId));
		}
		
		for(Public_Knowledge_Product_Display_Setting__c knSetting : knowledgeSettingProd)
		{
			String catName = knSetting.name;
			
			for(String dataCat : prodNameProdCate.get(catName))
			{
				if(catNameFAQs.containsKey(dataCat))
				{
					displayFAQResults.add(dataCat);
				}
			}
		}
	}
	
	public void constructMetadata()
	{
		knowledgeMetaSetting = knowledgeURL.get('Search');
		
		integer	sizeMetaTitle = 70;
		String normTitleMeta = '';
		String normDescMeta = '';
		if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
		{
			sizeMetaTitle -= knowledgeMetaSetting.SERP_Title__c.length() + 1;
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_Title__c))
		{
			normTitleMeta = knowledgeMetaSetting.Meta_Title__c;
			if(normTitleMeta.length() > sizeMetaTitle)
			{
				normTitleMeta = normTitleMeta.substring(0, sizeMetaTitle);
			}
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
		{
			normTitleMeta +=  ' ' + knowledgeMetaSetting.SERP_Title__c;
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_Description__c))
		{
			normDescMeta  = knowledgeMetaSetting.Meta_Description__c;
			if(normDescMeta.length() > 155)
			{
				normDescMeta 		 +=  normDescMeta.substring(0, 155);
			}
		}
		
		titleMeta 			= normTitleMeta;
		descriptionMeta		= normDescMeta;
		
		sizeMetaTitle = 70;
		normTitleMeta = '';
		normDescMeta = '';
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Title__c))
		{
			normTitleMeta = knowledgeMetaSetting.Meta_OG_Title__c;
			if(normTitleMeta.length() > sizeMetaTitle)
			{
				normTitleMeta = normTitleMeta.substring(0, sizeMetaTitle);
			}
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.SERP_Title__c))
		{
			normTitleMeta +=  ' ' + knowledgeMetaSetting.SERP_Title__c;
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Description__c))
		{
			normDescMeta  = knowledgeMetaSetting.Meta_OG_Description__c;
			if(normDescMeta.length() > 155)
			{
				normDescMeta 		 +=  normDescMeta.substring(0, 155);
			}
		}
		
    	titleMetaOG 		= normTitleMeta;
    	descriptionMetaOG		= normDescMeta;
    	
    	if(String.IsNotBlank(knowledgeMetaSetting.Meta_Keyword__c))
		{
			keywordsMeta = knowledgeMetaSetting.Meta_Keyword__c;	
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_URL__c))
		{
			urlMetaOG = knowledgeMetaSetting.Meta_OG_URL__c;	
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Description__c))
		{
			descriptionMetaOG = knowledgeMetaSetting.Meta_OG_Description__c;	
		}
		
		if(String.IsNotBlank(knowledgeMetaSetting.Meta_OG_Image__c))
		{
			imageMetaOG	= knowledgeMetaSetting.Meta_OG_Image__c;	
		}
	}
}