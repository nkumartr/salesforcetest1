global class ININBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable{
    String OAUT_TOKEN;
    public static final String CONST_COMPLETED = 'Completed';
    public List<Task> lstTask{get;set;}
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query='SELECT Id,Name FROM ININWebservice__c order by name LIMIT 10';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
            for(sObject so : scope){
                ININWebservice__c oININWebservice = (ININWebservice__c)so;
                ININServices.getFailedInteractions(OAUT_TOKEN, Integer.valueOf(oININWebservice.Name));
           }  
  
    }
    global void finish(Database.BatchableContext BC){
    }
    global void execute(SchedulableContext SC){
        Database.executebatch(this, 1);
    }   
}