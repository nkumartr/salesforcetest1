/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 18/0/2016
  @Description : Controller Class for the Belong Heading component .
*/
public with sharing class BelongHeadingController 
{
    public BelongSupportHomeController 			BlngCntrl 		{ get; set; }
    public BelongSupportSearchResultsController BlngSrchCon 	{ get; set; }
    public BelongSupportArticleController   	BlngArtCntrl 	{ get; set; }
    public BelongSupportCategoryController   	BlngCatCntrl 	{ get; set; }
    public Map<String,Public_Knowledge_URL_Mapping__c> knowledgeURL	{ get; set; }
    public String redirectURL									{ get; set; }
        
    public String searchTerms { get; set; }
    
    private KnowledgeSiteURLRewriter kwWritter;
    // Needed only because we are providing a second constructor
	public BelongHeadingController() 
	{
		knowledgeURL = KnowledgeSiteUtil.buildURLMapping();
		
		redirectURL = '';
	}
    
    public void buildSearchURL()
    {
    	//BlngCntrl.buildSearchURL();
		system.debug(logginglevel.error, 'BelongSupportHomeController @buildSearchURL');
		List<PageReference> BelongSupportUrls = new List<PageReference>();
		String URL = knowledgeURL.get('Search').Visualforce_Page_name__c + '?search=' + searchTerms;
		PageReference pageRedirect = new PageReference(URL);
		BelongSupportUrls.add(pageRedirect);
		
		if(kwWritter == Null)
			kwWritter = new KnowledgeSiteURLRewriter();
		
		BelongSupportUrls = kwWritter.generateUrlFor(BelongSupportUrls); 
		redirectURL = BelongSupportUrls[0].getURL();
		
		//redirectURL = 'Search?search=' + searchTerms;
		
		system.debug(logginglevel.error, 'BelongSupportHomeController @redirectURL ' + redirectURL);
		return;
		
    }
}