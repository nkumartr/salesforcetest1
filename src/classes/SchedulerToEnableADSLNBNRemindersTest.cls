/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/03/2016
  @Description : Test class for SchedulerToEnableADSLNBNReminders class
*/
@isTest
private class SchedulerToEnableADSLNBNRemindersTest 
{
	@testSetup static void setupTestData() 
	{
		Map<Id,Schema.RecordTypeInfo> caseRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
	    Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName		= GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
	    Map<Id,Schema.RecordTypeInfo> accRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
	    Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName		= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
		
		
		Notification_Settings__c settings = Notification_Settings__c.getOrgDefaults();
		settings.First_Reminder_Before_Disconnection__c = 90;
		settings.Second_Reminder_Before_Disconnection__c = 60;
		settings.Third_Reminder_Before_Disconnection__c = 30;
		settings.Fourth_Reminder_Before_Disconnection_del__c = 14;
		upsert settings;
		
		Account sAccount 		= new Account();
        sAccount.RecordTypeId   = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name           = 'Test Account';
        sAccount.Phone          = '0412345678';
        sAccount.Customer_Status__c      = 'Active';
        sAccount.Octane_Customer_Number__c = '123456';
        insert sAccount;
        
        Contact sContact       	= new Contact();
        sContact.FirstName  	= 'Joe';
        sContact.LastName  		= 'Belong';
        sContact.AccountId  	= sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        List<Case> caseList = New List<Case>();
        Case sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        scase.Belong_Id__c = '123456';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(100);
        caseList.add(sCase);
        
        sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        scase.Belong_Id__c = '123456';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(80);
        caseList.add(sCase);
        
        sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        scase.Belong_Id__c = '123456';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(50);
        caseList.add(sCase);
        
        sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        scase.Belong_Id__c = '123456';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(20);
        caseList.add(sCase);
        
        sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        sCase.NBN_Service_Class__c = '1';
        sCase.Belong_Id__c = '123456';
        sCase.Reminder_Stage__c = 'Final Notice (SMS)';
        sCase.Reminder_Date__c = System.today();
        sCase.HFL_Disconnection_Date__c = System.today();
        sCase.RecordTypeId   = CaseRecordTypeInfoName.get('ADSL to NBN Transitions').getRecordTypeId();
        sCase.AccountId = sAccount.Id;
        sCase.ContactId = sContact.Id;
        caseList.add(sCase);
        
        insert caseList;
        
        caseList = [Select id, Reminder_Date__c,  Reminder_Stage__c, NBN_No_Compliant__c FROM Case];
        
        for(Case caseObj: caseList)
        	caseObj.Reminder_Date__c = system.today();
        update caseList;
        caseList = [Select id, Reminder_Date__c,  Reminder_Stage__c, NBN_No_Compliant__c FROM Case];
	}
	
    static testMethod void EnableADSLNBNRemindersTest() 
    {
        Test.StartTest();
        SchedulerToEnableADSLNBNReminders sh1 = new SchedulerToEnableADSLNBNReminders();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Enable ADSL NBN Reminders', sch, sh1);
        Test.stopTest();
    }
}