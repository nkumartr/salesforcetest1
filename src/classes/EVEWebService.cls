/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 26/04/2016
  @Description : Class for interacting with EVE (Service Layer) in order to perform DML operations
                 to Salesforce and retrieve data from salesforce.
*/
global class EVEWebService 
{

    public class ShippingAddress
    {
        public String address1;
        public String address2;
        public String suburb;
        public String state;
        public String postcode;
    }

    WebService static String SMSResponseTask(List<String> responseSMS)
    {
        List<Task> updateSMSTask = new List<Task>();
        String response;
        try
        {
            response = 'Task being processed';
            for(String taskId : responseSMS)
            {
                List<String> smstask  = taskId.split('::');

                Task taskObj = new Task(id = smstask[0], SMS_Send_Status__c = smstask[1], SMS_Eve_Response__c = smstask[2]);

                if(smstask[1] == 'Success')
                {
                    taskObj.Notification_Date_Time__c = DateTime.now();
                }

                updateSMSTask.add(taskObj);
            }
            
            if(!updateSMSTask.isEmpty())
            {
                update updateSMSTask;
                response = 'Task Updated';
            }
        }
        catch(Exception e)
        {
            response = e.getMessage();
        }
        
        return response;
    }
    
    WebService static String createMoveCase(Case caseObj, Asset astObj, String accountId, String AgentId, String shipAddrs)
    {
        Asset astObjEmpty = new Asset(); //ONLY for comparison with parameter "astObj". even null is passed, webservice make it empty.
        ShippingAddress shippingAddress = parseAddress(shipAddrs);

        system.debug(logginglevel.error, 'shippingAddress ' + shippingAddress.address1 + ' ' + shippingAddress.address2 + ' '
                    + shippingAddress.state + ' ' + shippingAddress.postcode + ' '+ shippingAddress.suburb);

        Case caseMoveObj = EVEWebService.createCase(caseObj, accountId, 'Move Request', true);
        insert caseMoveObj;

        if(shippingAddress != null && String.isNotBlank(shippingAddress.address1) && caseMoveObj.contactId != null)
        {
            Contact contObj = new Contact(id = caseMoveObj.contactId);
            contObj.OtherStreet =  shippingAddress.address1 + (String.isNotBlank(shippingAddress.address2)? shippingAddress.address2: '');
            contObj.OtherCity =  shippingAddress.suburb;
            contObj.OtherCountry = 'Australia';
            contObj.OtherState = shippingAddress.state;
            contObj.otherPostalCode = shippingAddress.postcode;

            update contObj;
        }

        system.debug(logginglevel.error, 'astObj ' + astObj);
        if(astObj != NULL && astObj != astObjEmpty)
        {
            
            astObj.AccountId = caseMoveObj.AccountId;
            insert astObj;
        }
        system.debug(logginglevel.error, 'caseMoveObj ' + caseMoveObj);
        
        caseMoveObj = [Select id, CaseNumber From Case Where id =: caseMoveObj.Id LIMIT 1];
        system.debug(logginglevel.error, 'caseMoveObj ' + caseMoveObj);
        
        return caseMoveObj.CaseNumber;
    }

    private static ShippingAddress parseAddress(String shipAddrs) { 

        ShippingAddress addressObj = (ShippingAddress)JSON.deserializeStrict(
            shipAddrs, ShippingAddress.class);
        return addressObj;
    }

    private static Case createCase(Case caseObj, String accountId, String caseType, boolean assgnRule)
    {
        Account accObj = (Account) Cache.getRecordsFromCustomerNumber(accountId, new List<String>{'Contact'});
        caseObj.AccountId = accObj.Id;
        
        for(Contact contObj : accObj.Contacts)
        {
            if(contObj.Contact_Role__c == 'Primary')
            {
                caseObj.contactId = contObj.Id;             
            }
        }
       // if(caseObj.subject==null || CaseObj.Subject==''){
           /* Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
            caseRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
            caseObj.RecordTypeId    = caseRecordTypeInfoName.get(caseType).getrecordTypeId();*/
        //}
        if(assgnRule)
        {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;
            caseObj.setOptions(dmo);
        }
        
        return caseObj;
        
    }
    
    
    WebService static Case getMoveCaseCustomerNumber(String accountId, String caseNumber)
    {
       try{
           Account accObj = (Account) Cache.getRecordsFromCustomerNumber(accountId, new List<String>{'Case'});
            
            Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
            caseRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
            Id RecordTypeId         = caseRecordTypeInfoName.get('Move Request').getrecordTypeId();
            Id SwitchRecordTypeId   = caseRecordTypeInfoName.get('NBN Switch').getrecordTypeId();
            
            for(Case caseObj : accObj.Cases)
            {
                if((caseObj.RecordTypeId == RecordTypeId || caseObj.RecordTypeId == SwitchRecordTypeId) && caseObj.CaseNumber == caseNumber) 
                    return caseObj;
            }
            
            return null;
        }catch(exception e){
            return null;
        }
        
    }

    WebService static List<String> updateIFBOTCases(List<Case> caseList) 
    {
        Set<String> octaneRefNumberSet = new Set<String>();     
        Map<String,Case> mapOfOrderAndCase = new Map<String,Case>();
        Map<String,String> mapOfCaseIdOctaneNumber = new Map<String,String>();
        List<Case> updateCaseList = new List<Case>();
        List<CaseComment> updateCaseComList = new List<CaseComment>();
        List<CaseComment> finalUpdateCaseComList = new List<CaseComment>();
        
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
        caseRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
        Id RecordTypeCaseId     = caseRecordTypeInfoName.get('IFBOT').getrecordTypeId();

        for(Case caseObj : caseList)
        {
            octaneRefNumberSet.add((String)caseObj.Belong_Id__c);
            caseObj.RecordTypeId = RecordTypeCaseId;
            mapOfOrderAndCase.put(caseObj.Belong_Id__c, caseObj);
        }
        System.debug(logginglevel.error, 'This is the map ' + mapOfOrderAndCase);
        System.debug(logginglevel.error, 'This is the set ' + octaneRefNumberSet);
        
        List<Case> newCaseList = new List<Case>();
        Datetime followUpDate = GlobalUtil.AddBusinessDays(system.Now(), 1);

        for(Account accObj: [Select id, Octane_Customer_Number__c, 
                                        (SELECT id, Order_Health__c, Service__c, SLA_Missed__c, 
                                        Unhealthy_Order_Status__c, Follow_Up_Date__c,
                                        Unhealthy_Status_Reason__c, TELSTRA_REFERENCE__c, 
                                        Order_Reference__c, Customer_Octane_Number__c
                                        FROM Cases
                                        WHERE recordTypeId =: RecordTypeCaseId  AND Type = 'NBN'                                
                                        ORDER BY createdDate DESC
                                        )
                                FROM Account WHERE Octane_Customer_Number__c IN: octaneRefNumberSet])
        {
            Case caseOrder = mapOfOrderAndCase.get(accObj.Octane_Customer_Number__c);
            octaneRefNumberSet.remove(accObj.Octane_Customer_Number__c);


            if(accObj.Cases.isEmpty())
            {                               
                /*newCaseList.add(new Case(TELSTRA_REFERENCE__c = caseOrder.TELSTRA_REFERENCE__c, 
                                    Order_Health__c = caseOrder.Order_Health__c, Service__c = caseOrder.Service__c,
                                    SLA_Missed__c = caseOrder.SLA_Missed__c, Unhealthy_Order_Status__c = caseOrder.Unhealthy_Order_Status__c,
                                    Unhealthy_Status_Reason__c = caseOrder.Unhealthy_Status_Reason__c,
                                    AccountId = accObj.Id , Type = 'NBN', Priority='High', 
                                    Customer_Comments__c =  caseOrder.Customer_Comments__c,
                                    RecordTypeId = RecordTypeCaseId)); //Status = 'In Progress', Follow_Up_Date__c = followUpDate,*/
            }
            else if(accObj.Cases.size() == 1)
            {
                System.debug(logginglevel.info, 'This is the accObj.Cases ' + accObj.Cases);
                
                Case caseObj = accObj.Cases[0];

                if(accObj.Cases[0].Follow_Up_Date__c ==  NULL)
                    caseObj.Follow_Up_Date__c = followUpDate;

                updateCaseList.add(new Case(id = caseObj.id, TELSTRA_REFERENCE__c = caseOrder.TELSTRA_REFERENCE__c, 
                                    Order_Health__c = caseOrder.Order_Health__c, Service__c = caseOrder.Service__c,
                                    SLA_Missed__c = caseOrder.SLA_Missed__c, Unhealthy_Order_Status__c = caseOrder.Unhealthy_Order_Status__c,
                                    Unhealthy_Status_Reason__c = caseOrder.Unhealthy_Status_Reason__c )); //Status = 'In Progress', Follow_Up_Date__c =caseObj.Follow_Up_Date__c

                mapOfCaseIdOctaneNumber.put(caseObj.id,caseObj.Customer_Octane_Number__c);

                if(String.isNotEmpty(caseOrder.Customer_Comments__c))
                    updateCaseComList.add(new CaseComment(ParentId = caseObj.id, CommentBody = caseOrder.Customer_Comments__c));

            }
            else
            {
                boolean found = false;
                Case recentCaseObj = accObj.Cases[0];
                Case tempCaseObj;

                System.debug(logginglevel.info, 'This is the accObj.Cases ' + accObj.Cases);

                if(recentCaseObj.Service__c != caseOrder.Service__c)
                {
                    
                    for(integer i =1; i < accObj.Cases.size(); i++)
                    {
                        if(accObj.Cases[i].Service__c == caseOrder.Service__c)
                        {
                            recentCaseObj = accObj.Cases[i];
                            found = true;
                            break;
                        }
                        else if(accObj.Cases[i].TELSTRA_REFERENCE__c == caseOrder.TELSTRA_REFERENCE__c)
                        {   
                            tempCaseObj = accObj.Cases[i];
                        }

                    }

                    if(tempCaseObj != null && !found)
                    {
                        recentCaseObj = tempCaseObj;
                    }

                }

                if(recentCaseObj.Follow_Up_Date__c ==  NULL)
                    recentCaseObj.Follow_Up_Date__c = followUpDate;

                updateCaseList.add(new Case(id = recentCaseObj.id, TELSTRA_REFERENCE__c = caseOrder.TELSTRA_REFERENCE__c, 
                                    Order_Health__c = caseOrder.Order_Health__c, Service__c = caseOrder.Service__c,
                                    SLA_Missed__c = caseOrder.SLA_Missed__c, Unhealthy_Order_Status__c = caseOrder.Unhealthy_Order_Status__c,
                                    Unhealthy_Status_Reason__c = caseOrder.Unhealthy_Status_Reason__c)); // ,Status = 'In Progress', Follow_Up_Date__c =recentCaseObj.Follow_Up_Date__c

                mapOfCaseIdOctaneNumber.put(recentCaseObj.id,recentCaseObj.Customer_Octane_Number__c);

                if(String.isNotEmpty(caseOrder.Customer_Comments__c))
                    updateCaseComList.add(new CaseComment(ParentId = recentCaseObj.id, CommentBody = caseOrder.Customer_Comments__c));
            }
        
        }

        
        List<String> dmlErrors = New List<String>();
        Set<id> caseIdErrors = new Set<Id>();
        String ExceptionError = ''; 

        CaseTriggerHandler.executeTrigger = false;
        if(!updateCaseList.isEmpty())
        {
            System.debug(logginglevel.info, 'This is the updateCaseList ' + updateCaseList);
            Database.SaveResult[] updateResults = Database.Update(updateCaseList, false);

            // Iterate through each returned result
            for(integer i =0; i < updateCaseList.size(); i++) 
            {
                if (updateResults[i].isSuccess()) 
                {
                    // Operation was successful, so get the Customer_Octane_Number__c of the record that was processed
                    System.debug(logginglevel.Info, 'Successfully updated case with Id: ' + updateCaseList[i].Id);

                }
                else 
                {
                    caseIdErrors.add(updateResults[i].getId());

                    ExceptionError = 'Octane Customer Number => ' + mapOfCaseIdOctaneNumber.get(updateCaseList[i].Id);
                    
                    // Operation failed, so get all errors                
                    for(Database.Error err : updateResults[i].getErrors()) 
                    {
                        ExceptionError += ' - Exception Thrown => ' + err.getMessage();
                        
                    }
                    
                    dmlErrors.add(ExceptionError);
                }
            }       
                        
            CaseTriggerHandler.executeTrigger = true;

            if(!updateCaseComList.isEmpty())
            {   
                if(!caseIdErrors.isEmpty())
                {
                    for(CaseComment caseCommentsObj: updateCaseComList)
                    {
                        if(!caseIdErrors.contains(caseCommentsObj.ParentId))
                        {
                            finalUpdateCaseComList.add(caseCommentsObj);
                        }
                    }
                }
                else
                {
                    finalUpdateCaseComList.addAll(updateCaseComList);
                }
            }
        }

        if(!octaneRefNumberSet.isEmpty()){

            for(String octaneNumber: octaneRefNumberSet)
            {
                ExceptionError = 'Octane Customer Number => ' + octaneNumber + ' - Exception Thrown => ' + 'Not found in salesforce';
                dmlErrors.add(ExceptionError);
            }
        }

        if(!newCaseList.isEmpty())
        {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;
            
            for(Case caseObj: newCaseList)
            {
                caseObj.setOptions(dmo);
            }
            
            Insert newCaseList;

            for(Case caseObj: newCaseList)
            {
                String caseComments = 'Record created through Healthy/UnHealthy order integration due to record was not found. ' 
                                    + caseObj.Customer_Comments__c;
                finalUpdateCaseComList.add(new CaseComment(ParentId = caseObj.id, CommentBody = caseComments));
            }
        }

        if(!finalUpdateCaseComList.isEmpty())
            insert finalUpdateCaseComList;

        return dmlErrors;
       
    }

    WebService Static String retrieveActivities(String octaneCustomer, integer offsetNumber, integer size, String[] filterTypes) 
    {
        return JSON.serialize(InteractionHistoryUtil.retrieveActivities(octaneCustomer, offsetNumber, size, filterTypes));

    }

    WebService Static Void addInteraction(String utiliBillId, String type, String subject, String content,
            String activityType, String activitySubType, String contactLogId,
            String contactLogUsername,
            String email,
            String mobile, DateTime sentTime) 
    {
        InteractionHistoryUtil.addInteraction(utiliBillId, type, subject, content,
            activityType, activitySubType, contactLogId, contactLogUsername, email, mobile, sentTime);

    }

    Webservice Static void createSMSTaskforNotification(String octaneCustomerNumber, String notificationType,
                            String eventCode, List<String> fieldValuePair)
    {
        Map<String, String> fieldValueMap = new Map<String,String>();
        Account accObj = (Account) Cache.getRecordsFromCustomerNumber(octaneCustomerNumber, new List<String>{'Contact'});
        
        if(notificationType == 'SMS')
        {
            SMS_Template__c smsTemplate = (SMS_Template__c) cache.getRecordFieldSetValue('SMS_Template__c', 'Code__c', new Set<String>{eventCode});
            
            //Get Task parameters
            Task taskSMS = new Task();
            taskSMS.WhatId = accObj.Id;
            taskSMS.status = 'Completed';
            taskSMS.SMS_Send_Status__c = 'Send';
            taskSMS.Subject = smsTemplate.Name;
            Map<String,Schema.RecordTypeInfo> taskRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Task');
            taskSMS.RecordTypeId    = taskRecordTypeInfoName.get('SMS').getrecordTypeId();
            
            for(Contact conObj : accObj.Contacts)
            {
                if(conObj.Contact_Role__c == 'Primary')
                {
                    taskSMS.WhoId = conObj.Id;
                    taskSMS.SMS_Recipient__c = String.valueof(conObj.MobilePhone);
                    break;
                }
            }
            //Field = > fild value
            fieldValueMap = GLobalUtil.getHashMap(fieldValuePair);
            taskSMS.Description = GlobalUtil.mergeSMSTemplateWithFields(smsTemplate, fieldValueMap);
            
            insert taskSMS;
            
        }
        else if(notificationType == 'Email')
        {
            //Do something else
        }
    }

    WebService Static List<String> updateOrderServiceInfo(List<SFWrapperObject.OrderStatus> request) 
    {
        Map<String, SFWrapperObject.OrderStatus> octaneOrderStatusMap = new Map<String,SFWrapperObject.OrderStatus>();
        List<Order> orderList = new List<Order>();
        List<String> dmlErrors = new List<String>();

        for(SFWrapperObject.OrderStatus statusObj : request)
        {
            octaneOrderStatusMap.put(statusObj.octaneNumber, statusObj);
        }

        if(!octaneOrderStatusMap.keySet().isEmpty())
        {  
            List<Account> accountList = (List<Account>)Cache.getRecordsFromCustomerNumber(octaneOrderStatusMap.keySet(), new List<String>{'Order'});
            
            Order orderObj;

            for(Account accountObj : accountList)
            {
                if(!accountObj.Orders.isEmpty())
                {
                    orderObj = new Order(Id = accountObj.Orders[0].Id);
                    orderObj.Activation_Date__c = octaneOrderStatusMap.get(accountObj.Octane_Customer_Number__c).activationDate;
                    orderObj.isActive__c = octaneOrderStatusMap.get(accountObj.Octane_Customer_Number__c).isActive;
                    orderList.add(orderObj);
                }
                else
                {
                    String ExceptionError = 'Octane Customer Number => ' + accountObj.Octane_Customer_Number__c;
                    ExceptionError += ' - Exception Thrown => Order not found in Salesforce';
                    dmlErrors.add(ExceptionError);
                }

            }
        }

        if(!orderList.isEmpty())
        {
            String ExceptionError ;

            //  update orderList;

            Database.SaveResult[] updateResults = Database.Update(orderList, false);

            // Iterate through each returned result
            for(integer i =0; i < updateResults.size(); i++) 
            {
                ExceptionError = '';

                if (updateResults[i].isSuccess()) 
                {
                    System.debug(logginglevel.Info, 'Successfully updated Order with Id: ' + orderList[i].Id);

                }
                else 
                {
                    ExceptionError = 'Order Id => ' + orderList[i].Id;
                    
                    // Operation failed, so get all errors                
                    for(Database.Error err : updateResults[i].getErrors()) 
                    {
                        ExceptionError += ' - Exception Thrown => ' + err.getMessage();
                        
                    }
                    
                    dmlErrors.add(ExceptionError);
                }
            }  
        }

        return dmlErrors;
    }    

    WebService Static List<String> sendNotificationsToCustomers(List<SFWrapperObject.Notification> notifications)
    {

        List<String> messagingErrors = new List<String>();
        List<Log__c> logs = new List<Log__c>();
        List<SFWrapperObject.Notification> ValidatedList = new List<SFWrapperObject.Notification>();
        List<SFWrapperObject.Notification> smsList = new List<SFWrapperObject.Notification>();
        List<SFWrapperObject.Notification> emailList = new List<SFWrapperObject.Notification>();

        Event_Matrix__C eventMatrixObj;

        Set<String> custNos = new Set<String>();

        for(SFWrapperObject.Notification notifObj: notifications)
        {
            if(String.isNotBlank(notifObj.octaneNumber) && notifObj.eventAttributes != null && !notifObj.eventAttributes.isEmpty())
            {
                ValidatedList.add(notifObj);
                custNos.add(notifObj.octaneNumber);

                if(String.isnotBlank(notifObj.braintreeResponse))
                {
                    notifObj.messageAttributes.add('failure_type:'+ BrainTreeResponseCodes.getFailureType(notifObj.braintreeResponse));
                }
            }
            else
            {
                if(String.isBlank(notifObj.octaneNumber))
                {
                    logs.add(GlobalUtil.createLog('ERROR', 'No Octane Customer number in request', 'sendNotificationsToCustomers',
                                                     JSON.Serialize(notifObj)));
                    messagingErrors.add(notifObj.octaneNumber + ' - No Octane Customer number in request');
                }
                else
                {
                    logs.add(GlobalUtil.createLog('ERROR', notifObj.octaneNumber + ' - Event attributes are null or Empty', 'sendNotificationsToCustomers',
                                                     JSON.Serialize(notifObj)));
                    messagingErrors.add(notifObj.octaneNumber + ' - Event attributes are null or Empty');
                }
            }
        }

        List<sObject> accountList = Cache.getRecordsFromCustomerNumber(custNos, new List<String>{'Contact'});

        Map<String, sObject> accountMap = new Map<String, Account>();

        for(sObject accObj: accountList)
        {
            accountMap.put(((Account)accObj).Octane_Customer_Number__c, (Account)accObj);
        }


        for(SFWrapperObject.Notification notifObj: ValidatedList)
        {
            notifObj.accountObj = (Account)accountMap.get(notifObj.octaneNumber);

            if(notifObj.accountObj != null)
            {
                List<String> allAttributes = new List<String>();
                allAttributes.addAll(notifObj.eventAttributes);
                allAttributes.addAll(notifObj.messageAttributes);
                eventMatrixObj = GlobalUtil.getTemplateDetails(allAttributes);
                
                System.debug('eventMatrixObj : '+ eventMatrixObj);
                if(eventMatrixObj != null)
                {
                    if(eventMatrixObj.notification_type__c == 'SMS')
                    {
                        notifObj.smsTemplateCode = eventMatrixObj.SMS_Template_Code__c;
                        smsList.add(notifObj);
                    }
                }
                else
                {
                    logs.add(GlobalUtil.createLog('ERROR', notifObj.octaneNumber + ' - Not able to select a unique template based on request', 
                                                    'sendNotificationsToCustomers', JSON.Serialize(notifObj)));                   
                    messagingErrors.add(notifObj.octaneNumber + ' - Not able to select a unique template based on request');
                }
            } 
            else
            {
                logs.add(GlobalUtil.createLog('ERROR', notifObj.octaneNumber + ' - Account not found', 
                                                    'sendNotificationsToCustomers', JSON.Serialize(notifObj)));          
                messagingErrors.add(notifObj.octaneNumber + ' - Account not found');                
            }
            
        }
        //send sms
        if(!smsList.isEmpty())
        {
            sendSMS(smsList);
        }

        if(!logs.isEmpty())
        {
            insert logs;
        }

        return messagingErrors;

    }

    public Static void sendSMS(List<SFWrapperObject.Notification> smsEventList)
    {
        List<Task> smsTaskList = new List<Task>();
        Map<String, String> fieldValueMap = new Map<String,String>();
        Map<String, SMS_Template__c> smsTemplatesMap = Cache.getAllSMSTemplates();
        Boolean isBillingFlag = false;
        String descriptionText = '';
        Task taskSMS;

        for(SFWrapperObject.Notification smsNotification: smsEventList)
        {
            SMS_Template__c smsTemplate = smsTemplatesMap.get(smsNotification.smsTemplateCode);
            isBillingFlag = false; 

            if(String.isNotBlank(smsNotification.braintreeResponse))
            {
                isBillingFlag = true;
            }

            fieldValueMap = GLobalUtil.getHashMap(smsNotification.messageAttributes);
            descriptionText = GlobalUtil.mergeSMSTemplateWithFields(smsTemplate, fieldValueMap);

            taskSMS = GlobalUtil.createSMSTask(smsNotification.accountObj, smsTemplate.Name, isBillingFlag, descriptionText);

            smsTaskList.add(taskSMS);
        }

        insert smsTaskList;
    }

    WebService Static List<String> updateOrderInfoAndNotifyCustomers(List<SFWrapperObject.Notification> notifications)
    {
        //declarations
        List<String> messagingErrors = new List<String>();
        List<Log__c> logs = new List<Log__c>();
        List<SFWrapperObject.Notification> validatedList = new List<SFWrapperObject.Notification>();
        List<SFWrapperObject.Notification> smsList = new List<SFWrapperObject.Notification>();
        Set<String> custNos = new Set<String>();
        Map<String,String> messageAttributesMap;
        Map<String,String> eventAttributesMap;
        List<Case> updatedCases = new List<Case>();

        try
        {
            //validation
            for(SFWrapperObject.Notification notifObj : notifications)
            {
                String errorMessage = GlobalUtil.validateNotificationRequest(notifObj);

                if(String.isBlank(errorMessage))
                {
                    custNos.add(notifObj.octaneNumber);
                    validatedList.add(notifObj);
                }
                else
                {
                    logs.add(GlobalUtil.createLog('ERROR', errorMessage, 'updateOrderInfoAndNotifyCustomers',
                                                         JSON.Serialize(notifObj)));
                    messagingErrors.add(errorMessage);
                }
            }

            if(!custNos.isEmpty())
            {
                List<sObject> accountList = Cache.getRecordsFromCustomerNumber(custNos, new List<String>{'Contact','Case'});

                Map<String, sObject> accountMap = new Map<String, Account>();

                for(sObject accObj: accountList)
                {
                    accountMap.put(((Account)accObj).Octane_Customer_Number__c, (Account)accObj);
                }

                for(SFWrapperObject.Notification notifObj : validatedList)
                {
                    notifObj.accountObj =  (Account)accountMap.get(notifObj.octaneNumber);
                    
                    if(notifObj.accountObj != null)
                    {
                        messageAttributesMap = GlobalUtil.getHashMap(notifObj.messageAttributes);
                        eventAttributesMap = GlobalUtil.getHashMap(notifObj.eventAttributes);
                        if (EventsUtil.validAppointmentYear(messageAttributesMap.get('appointment_date'), logs))
                        { 
                            EventsUtil.populateSMSTemplate(notifObj, smsList, accountMap, messageAttributesMap, eventAttributesMap,logs,messagingErrors);
                            EventsUtil.updateCaseFields(notifObj, updatedCases, messageAttributesMap,logs);
                        }
                        else

                        {
                            logs.add(GlobalUtil.createLog('ERROR', 'Invalid Appointment Year', 'updateOrderInfoAndNotifyCustomers',
                                                         JSON.Serialize(notifObj)));
                        messagingErrors.add(messageAttributesMap.get('appointment_date') + ' - Invalid Appointment Year');
                        }
                    }
                    else
                    {
                        logs.add(GlobalUtil.createLog('ERROR', 'Invalid Octane Customer Number', 'updateOrderInfoAndNotifyCustomers',
                                                         JSON.Serialize(notifObj)));
                        messagingErrors.add('Invalid Octane Customer Number');
                    }
                }

                //send sms
                if(!smsList.isEmpty())
                {
                    sendSMS(smsList);
                }
                if(!updatedCases.isEmpty())
                {
                    update updatedCases;
                }
            }
        }
        catch(Exception e)
        {
            logs.add(GlobalUtil.createLog('ERROR', e.getMessage().abbreviate(225), 'updateOrderInfoAndNotifyCustomers',
                                                         JSON.Serialize(notifications)));     
        }
        
        if(!logs.isEmpty())
        {
            insert logs;
        }

        return messagingErrors;
    }
}