/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  			Sathish Boopathy (SathishBabu.Boopathy@team.telstra.com)
  @created : 27/05/2016
  @Description : Test Class for the BelongSupportCategoryController.
*/
@isTest
private class BelongSupportCategoryControllerTest {

    @testSetup static void setupTestData() 
    {
    	FAQ__kav faq = new FAQ__kav();
    	faq.title = 'test title';
    	faq.URLNAME = 'testUrl';
    	faq.Language  = 'en_US';
    	insert faq;
    	
    	TestUtil.createKnowledgeSettings();
    }

    static testmethod void testBelongSupportCategoryController()
    {
    	Test.startTest();
    	FAQ__kav faq = new FAQ__kav();
    	PageReference categoryPage = Page.Belong_Support_Category;
    	Test.setCurrentPage(categoryPage);
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(faq);
    	ApexPages.currentPage().getParameters().put('cat','Voice01');
    	BelongSupportCategoryController controller = new BelongSupportCategoryController(sc);
    	
    	String testStr;
    	testStr = controller.pkbCatCon.pageTitle;
    	testStr = controller.siteName;
    	testStr = controller.publishStatus;
    	boolean x = controller.isSite;
    	Test.stopTest();
    }
}