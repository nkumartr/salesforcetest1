/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/04/2016
  @Description : Class for sending SMS messages.
*/
public with sharing class TaskSMSController 
{
    public Task taskSMS         				{set; get;}
    public String WhatId                		{set; get;}
    public String WhoId                 		{set; get;}
    public String smsTemplateID         		{set; get;}
    public String smsTemplateCat        		{set; get;}
    public String contactSelected        		{set; get;}
    public SMS_Template__c smsTemplate  		{set; get;}
    public Boolean showMessage          		{set; get;}
    public Boolean restrictTemplate     		{set; get;}
    public Boolean isContactSelected     		{set; get;}
    
    private User user;
    private String retUrl;
    
    public List<SelectOption> smsTempCatsOptions {set; get;}
    public List<SelectOption> SMSTemplates		 {set; get;}
    public List<SelectOption> contactsList		 {set; get;}
    
    public Set<String>	allowProfiles = new Set<String>
    									{'Belong Operations', 
    										'Belong Team Leader',
    										'System Administrator'};
    //Constructor
    public TaskSMSController() // 
    {
    	Map<String,Schema.RecordTypeInfo> taskRecordTypeInfoName;
		Map<Id,Schema.RecordTypeInfo> taskRecordTypeInfoId;
		smsTempCatsOptions = new List<SelectOption>();
		contactsList = new List<SelectOption>();
        
        //this.taskSMS = (Task)stdController.getRecord();
        taskRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Task'); //getting all Recordtype for the Sobject
	    taskRecordTypeInfoName		= GlobalUtil.getRecordTypeByName('Task');//getting all Recordtype for the Sobject
        //RecordTypeId = ApexPages.currentPage().getParameters().get('RecordType');

        Id RTId = taskRecordTypeInfoName.get('SMS').getrecordTypeId();
        WhatId = ApexPages.currentPage().getParameters().get('Id');
        taskSMS = new Task();
        smsTemplate = new SMS_Template__c();
        
        taskSMS.WhatId = WhatId;
        taskSMS.RecordTypeId = RTId;
		taskSMS.Subject = 'SMS ';
		taskSMS.OwnerId = UserInfo.getUserId();
		taskSMS.Status = 'Completed';
        taskSMS.SMS_Send_Status__c = 'Send';
        restrictTemplate = false;
        showMessage = false;
        
        Id idProfile = UserInfo.getProfileId();
    	
    	Profile p = [SELECT Id, Name FROM Profile WHERE Id =: idProfile];
    	
    	If(!allowProfiles.contains(p.Name))
    		restrictTemplate = true;
        
        getRelatedContacts();
		getContactPhone();
		getTemplateCategory();
    }
    
    public void getRelatedContacts()
    {
    	List<SObject> contacts = Cache.getRecordsFieldSetValue('Contact', 'AccountId', new Set<String>{WhatId});
    	contactsList.add(new selectOption('', '--None--'));
    	for(SObject conObj : contacts)
    	{
    		Contact con = (Contact) conObj; 
    		contactsList.add(new SelectOption(con.Id, con.Name));
    	}
    }
    
    public void getTemplateCategory()  
    {
    	Schema.DescribeFieldResult templateSMSCats = Schema.sObjectType.SMS_Template__c.fields.Template_Category__c;
    	smsTempCatsOptions.add(new selectOption('', '--None--'));
        for (Schema.PickListEntry smsCatPickVal : templateSMSCats.getPicklistValues())
	    {
	    	smsTempCatsOptions.add(new SelectOption(smsCatPickVal.getValue(), smsCatPickVal.getLabel()));
	    }
    }
    
    //SMS Template available
    public void getSMSTemplateList()  
    {
        smsTemplates = new List<selectOption>();
        smsTemplates.add(new selectOption('', '--None--'));
        
        if(String.IsBlank(smsTemplateCat))
        {
        	taskSMS.Description = '';
        	taskSMS.Subject = 'SMS';
        	return;
        }
        
        for(SMS_Template__c smsTemp : [SELECT id, name, SMS_Template__c, Available_to_FOH__c
        								FROM SMS_Template__c 
        								WHERE Active__c = true AND
        								Template_Category__c =:smsTemplateCat])
        {
        	if(restrictTemplate && !smsTemp.Available_to_FOH__c )
        		continue;
            
            smsTemplates.add(new selectOption(smsTemp.Id, smsTemp.name));
        }
    }
    
    //Mergue SMS template into the Task description
    public void getSMSBody()
    {
    	if(String.isEmpty(smsTemplateID))
    	{
    		taskSMS.Description = '';
    		taskSMS.Subject = 'SMS';
    		
    		return;
    	}
    	
        if(taskSMS.WhoId != null && taskSMS.WhatId != null && 
            taskSMS.WhoId.getSObjectType().getDescribe().getName()=='Contact') 
		{
        	smsTemplate = (SMS_Template__c) Cache.getRecord(smsTemplateID);
            String whatIdAPIName = taskSMS.WhatId.getSObjectType().getDescribe().getName();
            String description = GlobalUtil.mergeSMSTemplateWithSObject(smsTemplate.SMS_Template__c,whatIdAPIName,taskSMS.WhatId);
            taskSMS.Description = GlobalUtil.mergeSMSTemplateWithSObject(description,'Contact',taskSMS.WhoId);
            taskSMS.Subject = smsTemplate.Name;
		}
    }
    
    public void getContactPhone()
    {
    	isContactSelected = false;
    	System.Debug(loggingLevel.Error, '@cache contactSelected ' + contactSelected);
        if (String.IsNotBlank(contactSelected))
        {
            Contact con = (Contact)Cache.getRecord(contactSelected);
            taskSMS.WhoId = con.id;
            System.Debug(loggingLevel.Error, '@cache Contact ' + con);
            if (String.IsNotBlank(con.MobilePhone))
            {
            	taskSMS.SMS_Recipient__c = String.valueof(con.MobilePhone);
            }
            isContactSelected = true;
        }
        else
        {
        	taskSMS.WhoId = Null;
        	taskSMS.SMS_Recipient__c = '';
        }
    }
    
    //Save task record
    public PageReference SendSMS()
    {   
        showMessage = false;
        if(String.isBlank(taskSMS.WhoId) )
        {
            showMessage = true;
            errorMsg('Please provide the Recipient of the SMS');
        }
        
        if(String.isBlank(taskSMS.WhatId))
        {
            showMessage = true;
            errorMsg('Please select a parent record for the SMS subject. i.e Account or Case');
        }
        
        if(taskSMS.WhoId == null || taskSMS.WhoId.getSObjectType().getDescribe().getName() != 'Contact')
        {
            showMessage = true;
            errorMsg('The SMS should be directed to a Contact');
        }
        
        if(String.isBlank(taskSMS.Description))
        {
            showMessage = true;
            errorMsg('Please select a SMS template');
        }
        
        
        if(showMessage)
            return null;
        
        String Message = 'Record Saved';
        infoMsg(Message);
        if(!Test.isRunningTest()) insert taskSMS;

        if (retURL == null) retURL = '/' + taskSMS.Id;
        PageReference ref = new PageReference(retURL);
        return ref;        

    }
    
    //Return to application record
    public PageReference Cancel()
    {
    	if (retURL == null) retURL = '/' + WhatId;
        PageReference ref = new PageReference(retURL);
        return ref;
    }
    public  void errorMsg(String s) { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, s));   }
    private void infoMsg(String s)  { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, s));    }
}