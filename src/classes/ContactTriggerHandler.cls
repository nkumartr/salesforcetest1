/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/06/2016
  @Description : Contact Trigger handler
*/

public with sharing class ContactTriggerHandler implements TriggerInterface
{
    private Boolean runAfterProcess = false;
    public static Boolean executeTrigger = true;
    
    /**
    *   This public method caches related data for before triggers, called once on start of the trigger execution
    */
    public void cacheBefore()
    {
        System.debug(LoggingLevel.ERROR, '====>cacheBefore is called');
        ContactTriggerHelper.initializeProperties();
        if(!(trigger.isDelete || trigger.isUnDelete))
        {
            ContactTriggerHelper.getPrimaryContact();
        }
        
    }

    /**
    *   This public method caches related data for after triggers, called once on start of the trigger execution
    */
    public void cacheAfter()
    {
        System.debug(LoggingLevel.ERROR, '====>cacheAfter is called');
        ContactTriggerHelper.initializeProperties();
        if(!(trigger.isDelete || trigger.isUnDelete))
        {
            ContactTriggerHelper.getPrimaryContact();
        }
    } 
    
    /**
    *   This public method process the business logic on 1 sObject at before insert
    */
    public void beforeInsert(sObject newSObj)
    {
        /*System.debug(LoggingLevel.ERROR, 'Contact handler beforeInsert is called');
        System.debug(LoggingLevel.ERROR,'====>ContactTriggerHandler.beforeInsert newSObj:' + newSObj);*/
        if(ContactTriggerHandler.executeTrigger)
        {
            ContactTriggerHelper.createGUID(newSObj);
            ContactTriggerHelper.verifyPrimaryContactExist(newSObj);
            
        }
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after insert
    */
    public void afterInsert(sObject newSObj)
    {
        if(ContactTriggerHandler.executeTrigger)
        {
            ContactTriggerHelper.verifyPrimaryContactDuplicate(newSObj);
            ContactTriggerHelper.changeNameOfAccount(newSObj);
            ContactTriggerHelper.createHashCode(newSObj);
             ContactTriggerHelper.restrictContactCreation(newSObj);
        }
    }
    
    /**
    *   This public method process the business logic on 1 sObject at before update
    */
    public void beforeUpdate(sObject oldSObj, sObject newSObj)
    {
        /*System.debug(LoggingLevel.ERROR,'====>ContactTriggerHandler.beforeUpdate');
        System.debug(LoggingLevel.ERROR,'====>ContactTriggerHandler.beforeUpdate newSObj:' + newSObj);
        System.debug(LoggingLevel.ERROR,'====>ContactTriggerHandler.beforeUpdate oldSObj:' + oldSObj);*/
        if(ContactTriggerHandler.executeTrigger)
        {
            ContactTriggerHelper.verifyPrimaryContactDuplicate(newSObj);
            contact oldC=(contact)oldSObj;
            contact newC=(contact)newSObj;
            if(oldC.authorized__c!=newC.authorized__c && newC.authorized__c)
                ContactTriggerHelper.restrictContactCreation(newSObj);
        }
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after update
    */
    public void afterUpdate(sObject oldSObj, sObject newSObj)
    {
        /*System.debug(LoggingLevel.ERROR,'====>ContactTriggerHandler.afterUpdate');
        System.debug(LoggingLevel.ERROR,'====>ContactTriggerHandler.afterUpdate newSObj:' + newSObj);
        System.debug(LoggingLevel.ERROR,'====>ContactTriggerHandler.afterUpdate oldSObj:' + oldSObj);*/
        if(ContactTriggerHandler.executeTrigger)
        {
            ContactTriggerHelper.changeNameOfAccount(oldSObj, newSObj);
        }
    }
    
    /**
    *   This public method process the business logic on 1 sObject at before delete
    */
    public void beforeDelete(sObject oldSObj)
    {
        
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after delete
    */
    public void afterDelete(sObject oldSObj)
    {
        
    }
    
    /**
    *   This public method process the business logic on 1 sObject at after undelete
    */
    public void afterUndelete(sObject newSObj)
    {}
    
    /**
    *   Ater all rows have been processed this method will be called
    *   Usualy DML are placed in this method
    */
    public void afterProcessing()
    {
        //Avoid calling DML if not required
        ContactTriggerHelper.updateAccountName();
        ContactTriggerHelper.updateContactHashCode();
        ContactTriggerHelper.afterProcessingResetInitialization();
        
    }   
}