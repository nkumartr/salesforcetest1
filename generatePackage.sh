#!/bin/bash


configDirectory=`pwd`/config

echo Generating package.xml from difference between master branch and Develop branch

if [ -d "$configDirectory" ]; then
        rm -r "$configDirectory"
fi




git diff remotes/origin/master remotes/origin/FeatureFix | force-dev-tool changeset create delta

rm -f $configDirectory/deployments/delta/destructiveChanges.xml

#echo Generating package.xml from difference between master branch and Develop branch
#generate_package()
#{
#old_changeset=$1
#new_changeset=git log --pretty=format:'%h' -n 1
#git diff $old_changeset $new_changeset | force-dev-tool changeset create delta
#}


#Function to check difference between changesets and generate package  file
#generate_package $1